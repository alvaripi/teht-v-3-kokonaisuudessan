import React,{Component} from "react";
import {Modal,Button,Row,Col,Form} from "react-bootstrap";

export class AddRegistration extends Component {

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        
        this.sate = {
            id:'',
            first:'',
            last: '',
            age:''

        }
      
    }

    handleSubmit(event){
        event.preventDefault();

        fetch("http://localhost:3000/lisaa",{
        method:"POST",
        headers:{
            "Accept":"application/json",
            "Content-Type" : "application/json"

        },
        body:JSON.stringify({               
            id : event.target.id.value,
            first : event.target.first.value,            
            last : event.target.last.value,                 
            age : event.target.age.value

        })   
        })
      .then(res=> res.json())
      .then((result) => 
      {
        alert("Onnistui");
      },
      (error) =>{
          alert("Failed")
      }
      ) 
        
    }
    
    render() {
        return (
            <Modal 
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Add registration 
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="container">
            <Row>
                <Col sm={8}>
                    <Form onSubmit={this.handleSubmit}>
                       
                    <Form.Group controlId="Id">
                            <Form.Label>Id</Form.Label>
                            <Form.Control
                            type="number"
                            
                            name="id"
                            required
                            placeholder="Id"
                            />
                            </Form.Group>
                   <Form.Group controlId="First">
                            <Form.Label>First</Form.Label>
                            <Form.Control
                            type="text"
                            
                            name="first"
                            required
                            placeholder="First"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="Last">
                            <Form.Label>Last</Form.Label>
                            <Form.Control
                            type="text"
                           
                            name="last"
                            required
                            placeholder="Last"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="Age">
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                            type="number"
                            
                            name="age"
                            required
                            placeholder="Age"
                            />
            
                        </Form.Group>
                        <Form.Group>
                            <Button variant='primary' type='submit' onClick={this.props.onhide}>Add reg</Button>
                        
                        </Form.Group>
                        
                        
                 
                    </Form>
                </Col>
            </Row>
             </div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        )
    }

}

export default AddRegistration