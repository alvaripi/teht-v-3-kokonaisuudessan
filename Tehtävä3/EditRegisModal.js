import React,{Component} from "react";
import {Modal,Button,Row,Col,Form} from "react-bootstrap";


export class EditRegisModal extends Component{

        constructor(props){
            super(props);
            this.handleSubmit = this.handleSubmit.bind(this);
        }

      
        handleSubmit(event){
            event.preventDefault();
            fetch("http://localhost:3000/paivita",{
            method:"PUT",
            headers:{
                "Accept":"application/json",
                "Content-Type" : "application/json"
    
            },
            body:JSON.stringify({               
              id : event.target.id.value,
              first : event.target.first.value,
              last : event.target.last.value,
              age : event.target.age.value
    
            })
            })
            .then(res=> res.json())
            .then((result) =>
            {
                    alert(result);
                    
            },
            (error)=>{
                alert("Onnistui");
                
            }
            )
    
            
                       
        }

        render() {
            return (
            
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                >
                <Modal.Header closeButton onClick={this.props.onhide}>
                <Modal.Title id="contained-modal-title-vcenter">
                Edit registration
                </Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                    <div className="container">
                    <Row>
                        <Col sm={6}>
                            <Form onSubmit={this.handleSubmit}>
                        <Form.Group controlId="Id" >
                        <Form.Label>Id</Form.Label>
                        <Form.Control
                            type="text"
                            id="Id"
                            name="id"
                            required
                            defaultValue = {this.props.id}
                            placeholder="Id"
                        />
            
                        </Form.Group>
                        <Form.Group controlId="First">
                        <Form.Label>First</Form.Label>
                        <Form.Control
                            type="text"
                            id="First"
                            name="first"
                            required
                            defaultValue = {this.props.first}
                            placeholder="First"
                        />
            
            </Form.Group>
            <Form.Group controlId="Last">
            <Form.Label>Last</Form.Label>
            <Form.Control
                    type="text"
                    id="Last"
                    name="last"
                    required
                    defaultValue = {this.props.last}
                    placeholder="Last"
                />
            
            </Form.Group>
            <Form.Group controlId="Age">
            <Form.Label>Age</Form.Label>
            <Form.Control
                    type="text"
                    id="Age"
                    name="age"
                    required
                    defaultValue = {this.props.age}
                    placeholder="Age"
                />
            
            </Form.Group>
            <Form.Group>
            <Button variant='primary' type='submit'>Update reg</Button>
            
            </Form.Group>
            
            </Form>
            </Col>
            </Row>
            </div>
            </Modal.Body>
             <Modal.Footer>
                <Button variant="danger" onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
            
            ) 
        }
        

}

export default EditRegisModal
