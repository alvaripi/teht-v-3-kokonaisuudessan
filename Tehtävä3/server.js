const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const PORT = 3000;
const HOST = '127.0.0.1';

const app = express();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors())

app.use(function(req, res, next) {

res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

next();

});





// Tehdaan yhteys
const db = mysql.createConnection({
    host : "localhost", 
    user : "root",
    password : "root",
    database : "db"
});


//yhdista

db.connect((err) => {
    if(err){
        throw err;
    }

    console.log("Yhdistetty");
});






// Lisaa tietoja 

app.post('/lisaa', (req, res) => {
    let sid = req.body.id;
    let sfirst = req.body.first;
    let slast = req.body.last;
    let sage = req.body.age;
        let registration = {id: sid, first: sfirst, last: slast, age: sage};
        let sql = 'INSERT INTO registration SET ?';
        let query = db.query(sql, registration, (err, result) => {
    if(err) throw err;
        console.log(result);
            res.send(result)
        });
    });

// Select
app.get('/select', (req, res) => {
    let sql = "SELECT * FROM registration";
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.send(results);
    });
});

// Valitse yksi tieto
app.get('/valitse/:id', (req, res) => {
    let sql = `SELECT * FROM registration WHERE id = ${req.params.id}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send("Valittu");
    });

});

// Päivitä tieto
app.put('/paivita', (req, res) => {
    let zid = req.body.id;
    let zfirst = req.body.first;
    let zlast = req.body.last;
    let zage = req.body.age;
    let registration = {id: zid, first: zfirst, last: zlast, age: zage};
   
    let sql = "UPDATE registration SET ? WHERE id = ?";
    let query = db.query(sql, [registration, zid], (err,result) => {
        if(err) throw err;
        console.log(result);
        res.send("Paivitetty");
    });

});

//poista tieto
app.delete('/poista/:id', (req, res) => {
    let sql = `DELETE FROM registration WHERE id = ${req.params.id}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send("Poistettu");
    });

});





app.listen(PORT, HOST, function() {
    console.log("server up and running @ http://" + HOST + ":" + PORT);
    });
module.exports = app;

// Luo taulu
/** 
 app.get("/luotaulu" ,(req,res) => {
    let sql ="CREATE TABLE posts(id int AUTO_INCREMENT, title VARCHAR(255), body VARCHAR(255), PRIMARY KEY(id) )";
    db.query(sql, (err, result) =>{
        if(err) throw err;
        console.log(result);
        res.send("Taulu luotu");

     });
}); 

*/

//Tehdaan tietokanta

/** 
app.get("/creatdb", (req, res) => {
    let sql = "CREATE DATABASE nodemysql";
    db.query(sql, (err,result) => {
        if(err) throw err;
        console.log(result);
        res.send("Database tehty");
    });
});  
*/