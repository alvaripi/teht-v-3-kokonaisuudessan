import React,{Component} from "react";
import {Table} from "react-bootstrap";

import {Button, ButtonToolbar} from "react-bootstrap";
import {AddRegistration} from "./AddRegistration";
import {EditRegisModal} from "./EditRegisModal";


export class Registrations extends Component {

    constructor(props){
        super(props);
        this.state ={regs : [], addModalShow : false, editModalClose : false}
    }

    componentWillMount() {
        this.refreshList();

    }

    refreshList() {
        //tämä päivittää näytön samalla

        fetch("http://localhost:3000/select")
        .then(res => res.json())
        .then((data) => {
        this.setState({regs:data});

        
        }
        
        
        );
        //tähän fetch 
    }

    /**componentDidUpdate(){
        this.refreshList();
    } */

    deleteReg(id)
    {
        if(window.confirm("Oletko varma?"))
        {
            fetch("http://localhost:3000/poista/" + id,{
            method:"DELETE",
            header:{ "Accept":"application/json",
            "Content-Type" : "application/json"    
            }
            })
        }   

    }

    render() {
    
        const {regs} = this.state;
        const {id, first, last, age} = this.state;
        let editModalClose = () => this.setState({editModalShow:false});
        let addModalClose = () => this.setState({addModalShow:false});

        return (
            <div>
                <Table>
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>First</th>
                        <th>Last</th>
                        <th>Age</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    {regs.map(reg=>
                    <tr>
                        <td>{reg.id}</td>
                        <td>{reg.first}</td>
                        <td>{reg.last}</td>
                        <td>{reg.age}</td>
                        <td>
                        
                       <ButtonToolbar>
                           <Button
                           className="mr-2" variant="info"
                           onClick={()=> this.setState({editModalShow:true, id:reg.id, first:reg.first, last:reg.last, age:reg.age })}
                           >
                               Muokkaa
                           </Button>
                        <EditRegisModal
                        show = {this.state.editModalShow}
                        onHide={editModalClose}
                        id ={id}
                        first={first}
                        last = {last}
                        age = {age}
                        />

                       </ButtonToolbar>
                        
                        <Button
                        variant="danger"
                        onClick ={() => this.deleteReg(reg.id)}
                        >Poista</Button>
                        </td>
                    </tr>
                    )
                    }
                   
                   </tbody>
                </Table>  
            <ButtonToolbar>
                <Button
                variant="primary"
                onClick={() => this.setState({addModalShow: true})}
                
                >Lisää Tieto</Button>
                
                <AddRegistration
                show={this.state.addModalShow}
                onHide={addModalClose}            
                />
            </ButtonToolbar>
            </div>
        )
    }

}

export default Registrations